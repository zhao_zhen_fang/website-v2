**Is this a BUG REPORT or FEATURE REQUEST?**:

> Uncomment only one, leave it on its own line: 
>
> - bug
>
> - feature


**What happened**:

**What you expected to happen**:

**How to reproduce it (as minimally and precisely as possible)**:

**Anything else we need to know?**:

**Environment**:
- Access terminal (Mobile or PC):
- Terminal bland (Only bugs that happen on the mobile or pad ):
- Browser brand and version (e.g Chrome 82):
- Operating system brand and version (e.g. windows 10):
- Others:
